/*
 * Copyright (C) 2015 - Michael Zanetti <michael.zanetti@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import WiFi 1.0

MainView {
    id: app
    applicationName: "wifiscanner.arubislander"

    Component.onCompleted: Theme.name = "Ubuntu.Components.Themes.SuruDark"

    readonly property var colors: ["red", "blue", "darkorchid", "white", "lime", "lightblue", "chartreuse", "cyan", "deeppink", "deepskyblue", "greenyellow", "lawngreen", "lime", "magenta", "snow", "yellow"]

    width: units.gu(40)
    height: units.gu(75)

    Iwlist {
        id: iwlist
    }

    Timer {
        running: Qt.application.active
        triggeredOnStart: true
        repeat: true
        interval: 15000
        onTriggered: iwlist.start();
    }

    ActionList {
         id: tabsList

         children: [
             Action {
                 text: i18n.tr("2.4 GHz")
                 iconName: "network-wifi-symbolic"
                 onTriggered: {
                     tabs.selectedTabIndex = 0
                 }
             },

             Action {
                 text: i18n.tr("5 GHz")
                 iconName: "network-wifi-symbolic"
                 onTriggered: {
                     tabs.selectedTabIndex = 1
                 }
             }
         ]
    }


    Tabs {
        id: tabs
        Tab {
            id: tab24
            title: i18n.tr("2.4 GHz")
            WiFiListPage {
                title: i18n.tr("WiFi Scanner - 2.4 GHz")
                band: Iwlist.WiFiBand24GHz
            }
        }

        Tab {
            id: tab5
            title: i18n.tr("5 GHz")
            page: WiFiListPage {
                title: i18n.tr("WiFi Scanner - 5 GHz")
                band: Iwlist.WiFiBand5GHz
            }
        }
    }
}

