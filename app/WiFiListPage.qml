/*
 * Copyright (C) 2015 - Michael Zanetti <michael.zanetti@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import WiFi 1.0

Page {
    id: root

    property alias band: canvas.band

    header: PageHeader {
        title: root.title
        leadingActionBar.actions: tabsList.actions
    }

    WiFiFilterModel {
        id: wifiModel
        sourceModel: iwlist
        band: root.band
    }

    Connections {
        target: iwlist
        onRunningChanged: canvas.requestPaint();
    }

    Flickable {
        id: flickable
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            bottom: detailsItem.top
            topMargin: root.header.height
        }

        contentHeight: height
        contentWidth: canvas.width

        Item {
            height: flickable.height
            width: band == Iwlist.WiFiBand24GHz ? flickable.width : units.gu(100)
            WifiCanvas {
                id: canvas
                scanning: iwlist.running
                wifiModel: wifiModel
                band: root.band
            }
        }
    }


    MouseArea {
        anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
        height: detailsItem.height
        property bool ignoring: false
        onPressed: {
            ignoring = false;
            if (detailsItem.showProgress == 0 && mouseY < height - units.gu(2)) {
                print("rejecting mouse")
                mouse.accepted = false;
                ignoring = true;
            }
        }
        onMouseYChanged: {
            if (ignoring) {
                return;
            }

            // height : 0 = dI.height : 1
            detailsItem.showProgress = (height - mouseY) / height
            print("sp", detailsItem.showProgress, detailsItem.anchors.verticalCenterOffset)
        }
        onReleased: {
            if (detailsItem.showProgress > .5) {
                detailsItem.showProgress = 1;
            } else {
                detailsItem.showProgress = 0;
            }
        }
    }

    Item {
        id: detailsItem
        anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
        height: root.height / 2
        anchors.bottomMargin: Math.min(Math.max(-height + units.gu(2), -height + showProgress * height), 0)
        property real showProgress: 0

        Behavior on anchors.bottomMargin {
            LomiriNumberAnimation {}
        }

        Rectangle {
            id: dragHandle
            anchors { left: parent.left; right: parent.right; top: parent.top }
            height: units.gu(2)
            color: Qt.lighter(Qt.lighter(backgroundColor))

            Row {
                anchors.centerIn: parent
                spacing: units.gu(1)
                Repeater {
                    model: 3
                    Rectangle {
                        height: units.gu(1)
                        width: height
                        radius: height / 2
                        color: Qt.lighter(Qt.lighter(Qt.lighter(Qt.lighter(backgroundColor))))
                    }
                }
            }
        }

        Rectangle {
            anchors { left: parent.left; right: parent.right; top: dragHandle.bottom; bottom: parent.bottom }
            color: Qt.lighter(backgroundColor)
        }
        ListView {
            anchors { left: parent.left; right: parent.right; top: dragHandle.bottom; bottom: parent.bottom }
            model: wifiModel
            clip: true

            onCountChanged: {
                if (iwlist.findWiFi(canvas.selectedHwAddr) == -1) {
                    canvas.selectedHwAddr = "";
                }
            }

            delegate: ListItem {
                id: delegate
                property var item: wifiModel.get(index)
                height: layout.height + (divider.visible ? divider.height : 0)

                onClicked: {
                    print("clicked:", index, canvas.selectedHwAddr)
                    if (canvas.selectedHwAddr == delegate.item.hwAddr) {
                        canvas.selectedHwAddr = "";
                    } else {
                        canvas.selectedHwAddr = delegate.item.hwAddr;
                    }
                    print("end clicked:", index, canvas.selectedHwAddr)
                }
                Rectangle {
                    anchors.fill: parent
                    color: "white"
                    opacity: delegate.item && canvas.selectedHwAddr == delegate.item.hwAddr ? 0.2 : 0
                }

                ListItemLayout {
                    id: layout
                    title.text: delegate.item ? delegate.item.name : ""
                    title.color: delegate.item ? app.colors[delegate.item.color % app.colors.length] : "white"
                    subtitle.text: "[" + delegate.item.hwAddr + "]"

                    Item {
                        id: slot
                        width: secondLabel.width
                        height: parent.height
                        //as we want to position labels to align with title and subtitle
                        SlotsLayout.overrideVerticalPositioning: true
                        Label {
                            id: firstLabel
                            anchors.right: secondLabel.right
                            text: delegate.item ? delegate.item.frequency.toFixed(3) + " (" + delegate.item.channel + ")" : ""
                            fontSize: "small"
                            y: layout.mainSlot.y + layout.title.y
                               + layout.title.baselineOffset - baselineOffset
                        }
                        Label {
                            id: secondLabel
                            text: delegate.item ? delegate.item.level + " dBm" : ""
                            fontSize: "small"
                            y: layout.mainSlot.y + layout.subtitle.y
                               + layout.subtitle.baselineOffset - baselineOffset
                        }
                    }
                }
            }
        }
    }
}
